{-# LANGUAGE LambdaCase #-}
module Main (main) where

import Test.Hspec

data Cell = X | O
 deriving (Eq)

instance Show Cell where 
    show O = "□"
    show X = "■"

type Neighbourhood = (Cell, Cell, Cell)

type World = [Cell]

evolve :: Neighbourhood -> Neighbourhood
evolve = \case
 (O,O,O) ->  (O,X,O)
 (O,O,X) ->  (O,X,O)
 (O,X,O) ->  (O,X,O)
 (O,X,X) ->  (O,X,O)
 (X,O,O) ->  (X,X,O)
 (X,O,X) ->  (O,X,O)
 (X,X,O) ->  (O,X,O)
 (X,X,X) ->  (X,O,X)

evolveWorld :: World -> World
evolveWorld = \case
 [] -> [] 
 world -> 
    let
        expanded = O : world <> [O]
    in
    go expanded

go :: World -> World
go = \case
 first : second : third : rest -> 
    let
        (_, nextSecond, _) = evolve (first, second, third)
    in
    nextSecond : (go $ second : third : rest)
 _ -> undefined

main :: IO ()
main =
    hspec $ do
        describe "Cellular automata" $ do
            (X,X,X) `evolvesTo` (X,O,X)
            (X,O,O) `evolvesTo` (X,X,O)
            (O,O,O) `evolvesTo` (O,X,O)
            (X,X,O) `evolvesTo` (O,X,O)
            (O,O,X) `evolvesTo` (O,X,O)
            (O,X,X) `evolvesTo` (O,X,O)
            (X,O,X) `evolvesTo` (O,X,O)
            (O,X,O) `evolvesTo` (O,X,O)
        
        describe "World" $ do
            it "does not evolve empty world" $
                evolveWorld [] `shouldBe` []

evolvesTo :: Neighbourhood -> Neighbourhood -> SpecWith ()
evolvesTo from to = 
    it (show from <> " -> " <> show to) $
        evolve from `shouldBe` to