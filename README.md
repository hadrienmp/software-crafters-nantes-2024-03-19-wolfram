# Automate cellulaire de Wolfram

Au meetup software crafters Nantes du 19 mars 2024. Implémentation d'une des règles de [ l'automate cellulaire élémentaire ](https://en.wikipedia.org/wiki/Elementary_cellular_automaton) en dur en Haskell.

You can use Nix to install

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#haskell" ./your_new_project_directory
```

---

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

---

[Awesome Haskell](https://github.com/krispo/awesome-haskell#readme)

[Hoogle](https://hoogle.haskell.org)
